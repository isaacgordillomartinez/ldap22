# LDAP SERVER 2022 EN DETACH
# Isaac Gordillo


Este es un contenedor de Docker para crear objetos de una base de datos de un desguace.

## EJECUCIÓN DEL CONTENEDOR

phpldapadmin

```
docker build -t isaacgm22/ldap22:phpldapadmin .
```

```
docker run --rm --name phpldapadmin.edt.org -h phpldapadmin.edt.org -p 80:80 --network 2hisx -d isaacgm22/ldap22:phpldapadmin
```

Práctica

```
docker build -t isaacgm22/ldap22:practica .
```

```
docker run --rm --name ldap.edt.org -h ldap.edt.org --network 2hisx -p 389:389 -d isaacgm22/ldap22:practica
```

**Archivos que no cambian respecto al contenedor base:**

- Dockerfile
- startup.sh

## CAMBIOS EN EL ARCHIVO SLAPD.CONF

Se han borrado todos aquellos schemas que no son utilizados. Aquí está la lista:

```
/etc/ldap/schema/corba.schema
/etc/ldap/schema/duaconf.schema
/etc/ldap/schema/dyngroup.schema
/etc/ldap/schema/java.schema              # Contenedores de java
/etc/ldap/schema/misc.schema
/etc/ldap/schema/openldap.schema
/etc/ldap/schema/ppolicy.schema           # Password policy
/etc/ldap/schema/collective.schema
```

Los que son esenciales para el funcionamiento del contenedor son los siguientes:

```
/etc/ldap/schema/core.schema              # Es el núcleo de todo
/etc/ldap/schema/cosine.schema            # Contiene dependencias de inetOrgPerson
/etc/ldap/schema/inetorgperson.schema     # Atributos de personas
/etc/ldap/schema/nis.schema               # Contiene posixAccount
/opt/docker/desguace.schema               # Nuevo esquema
```

## DESGUACE.SCHEMA

En este esquema hay dos clases de objetos, que servirán para definir las entidades de la base de datos **edt-org**. La primera, que **deriva de top** y es de tipo **STRUCTURAL**, se llama **x-desguace**.
La segunda también **deriva de top**, pero es de tipo **AUXILIARY** y se llama **x-vehiculo**.

- **IMPORTANTE**: Todas las objectClass de tipo AUXILIARY heredan de TOP. Además, toda entidad ha de tener un solo objeto estructural pero puede tener varios objetos auxiliares.

La idea es que estas clases de objetos no hereden ninguna característica de ninguna clase predefinida en LDAP. De esta forma se pueden amoldar a un ejemplo de la vida real como es un desguace.

El desguace es la clase objeto de tipo **estructural**, dado que un desguace puede ser definido por sí mismo. Por otro lado, la segunda clase de objeto, la **auxiliar**, contiene descripciones de los vehículos que van a ser despedazados, por lo que sus datos no son de alta importancia al acabar transformados en chatarra.

Sin embargo, el razonamiento que sostiene guardar las características de cada vehículo es que, de esta forma, se pueden realizar análisis sobre qué marcas y modelos tienden más a ser enviados a un desguace.

Una vez dicho esto, aquí está el resumen de la estructura del archivo **desguace.schema** antes de haber sido traducido a la sintaxis que requiere LDAP.

- objectClass de desguaces:

    ```
    ObjectClass: x-desguace
    Deriva de top, STRUCTURAL

    x-desguace:

    MUST:

        · x-identificador: integer

    MAY

        · x-nombre: string
        · x-localizacion: string
        · x-activo: boolean
        · x-horario: string
        · x-web: IA5 string
        · x-reparacion: boolean      # Indica si ofrece servicio de reparación o no.
    ```

- objectClass de vehículos:

    ```
    ObjectClass: y-vehiculo
    Deriva de top , AUXILIARY

    y-vehiculo:

    MUST:

        · y-matricula: string

    MAY:

        · y-tipo: string
        · y-marca: string
        · y-modelo: string
        · y-foto: JPEG
        · y-color: string
        · y-accidente: PDF           # Describe si ha sufrido un accidente y su tipo.
    ```

## CAMBIOS EN EL ARCHIVO EDT-ORG.LDIF

Se ha añadido una organizationalUnit nueva: **ou=practica**.

```
dn: ou=practica,dc=edt,dc=org
ou: practica
description: Seccion de desguaces de vehiculos.
objectclass: organizationalunit
```

Esta contiene 3 entidades nuevas, siendo cada una madre de otra más, habiendo 6 en total.

Al visualizar el contenido de este contenedor usando el protocolo **php** del otro contenedor yendo a **localhost:80/ldapadmin**, las entidades quedan clasificadas de esta manera:

- Desguace 1: Páramo del Martillo

    ```
    dn: x-identificador=69420,ou=practica,dc=edt,dc=org
    objectClass: x-desguace
    x-identificador: 69420
    x-nombre: Paramo del Martillo
    x-localizacion: C/ Gran Via, 1, Madrid
    x-activo: TRUE
    x-horario: De lunes a viernes, 08:00h-14:00h y 15:00h-21:00h
    x-web: www.desguaceparamodelmartillo.es
    x-reparacion: FALSE
    ```

    * Vehículo 1: Excavadora de rueda gigante TAKRAF Bagger 293 con matrícula GUTI 0000

        ```
        dn: y-matricula=GUTI 0000,x-identificador=69420,ou=practica,dc=edt,dc=org
        objectClass: y-vehiculo
        objectClass: x-desguace
        x-identificador: 69420
        y-matricula: GUTI 0000
        y-tipo: Excavadora de rueda gigante
        y-marca: TAKRAF
        y-modelo: Bagger 293
        y-foto:< file:///opt/docker/bagger.jpg
        y-accidente:< file:/opt/docker/bagger.pdf                      # IMPORTANTE: Si no se escribe el carácter '<' junto a ':' no funciona.
                                                                       # La ruta puede ser absoluta con una barra '/' o con tres '///' o bien
                                                                       # relativa a la carpeta que copia el container dentro suyo, que es desde
                                                                       # la cual trabaja
        ```

- Desguace 2: Túmulo de las chatarras lúgubres

    ```
    dn: x-identificador=69421,ou=practica,dc=edt,dc=org
    objectClass: x-desguace
    x-identificador: 69421
    x-nombre: Tumulo de las chatarras lugubres
    x-localizacion: C/ Gran Via, 1, Barcelona
    x-activo: FALSE
    x-reparacion: FALSE
    ```

    * Vehículo 2: Sedán Mercedes-Benz SLS-AMG con matrícula AE 1936

        ```
        dn: y-matricula=AE 1936,x-identificador=69421,ou=practica,dc=edt,dc=org
        objectClass: y-vehiculo
        objectClass: x-desguace
        x-identificador: 69421
        y-matricula: AE 1936
        y-tipo: Sedan
        y-marca: Mercedes-Benz
        y-modelo: SLS-AMG
        y-foto:< file:/opt/docker/mercedes.jpg
        y-color: plateado
        y-accidente:< file:/opt/docker/mercedes.pdf
        ```

- Desguace 3: Desguace Tozolón Tumbado

    ```
    dn: x-identificador=69422,ou=practica,dc=edt,dc=org
    objectClass: x-desguace
    x-identificador: 69422
    x-nombre: Desguace Tozolon Tumbado
    x-localizacion: C/ Algarabia, 1, Despeñaperros
    x-activo: TRUE
    x-horario: De lunes a viernes, 06:00h-13:00h y 14:00h-17:00h
    x-web: www.desguacetozolontumbado.es
    x-reparacion: TRUE
    ```

    * Vehículo 3: Helicóptero de combate Boeing AH-64 Apache con matrícula FCB 1899

        ```
        dn: y-matricula=FCB 1899,x-identificador=69422,ou=practica,dc=edt,dc=org
        objectClass: y-vehiculo
        objectClass: x-desguace
        x-identificador: 69422
        y-matricula: FCB 1899
        y-tipo: Helicoptero de combate
        y-marca: Boeing
        y-modelo: AH-64 Apache
        y-foto:< file:///opt/docker/apache.jpg
        y-color: Camuflaje desertico
        y-accidente:< file:apache.pdf
        ```

## ARCHIVOS .JPG Y .PDF

Cada vehículo tiene su propia imagen en formato **.jpg** y un informe en formato **.pdf** que describe cómo ha sido el accidente que ha sufrido en caso de que uno haya ocurrido.

```
apache.jpg
apache.pdf
mercedes.jpg
mercedes.pdf
bagger.jpg
bagger.pdf
```