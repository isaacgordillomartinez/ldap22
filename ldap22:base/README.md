# LDAP SERVER 2022 EN DETACH
# Isaac Gordillo


La idea es crear un container de Docker **en segundo plano** que contenga un servidor ldap.

Para esto, lo primero que hay que hacer es crear una carpeta (por ejemplo ~/docker) para dentro volcar archivos que usará el container, como los de configuración del servidor ldap **(slapd.conf y uno .ldif)** y el archivo **Dockerfile**, que indicará qué instrucciones deben seguirse al crear el contenedor. IMPORTANTE: el **Dockerfile** ha de tener este nombre exacto para que el sistema lo reconozca. Hay un ejemplo de su contenido tras esta explicación.

```
vim Dockerfile
```

## EXPLICACIÓN DE LO QUE CONTIENE EL Dockerfile

- De qué distribución y versión de Linux se va a crear el contenedor. En este caso, Debian.

```
FROM debian
```

- (Opcional) etiquetas.

```
LABEL subject="ldap server 2022"
LABEL author="Isaac"
```

- Una variable que evita que no se requiera de diálogo durante la instalación de las aplicaciones indicadas en el script.

```
ARG DEBIAN_FRONTEND=noninteractive
```

- Estas son las utilidades básicas para un servidor ldap. '&&' hace que se ejecuten las dos órdenes de forma simultánea. IMPORTANTE: las del servidor **ldap** son **slapd**, que es el demonio, y **ldap-utils**, que son sus herramientas.

```
RUN apt-get update && apt-get -y install procps tree nmap vim iproute2 iputils-ping slapd ldap-utils
  ```

- Crear un directorio dentro del container.

```
RUN mkdir /opt/docker
```

- Establecerlo como directorio activo.

```
WORKDIR /opt/docker
```

- Copiar el contenido de la carpeta donde se encuentra el **Dockerfile** dentro del container. IMPORTANTE: hay que añadir **'/'** al final.

```
COPY * /opt/docker/
```

- Dar permisos de ejecución (-x) al script **startup.sh** dentro de la carpeta **/opt/docker** del container.

```
RUN chmod +x /opt/docker/startup.sh
```

- Ejecutarlo.

```
CMD /opt/docker/startup.sh
```

- Abrir el puerto **389**, que es de **ldap**.

```
CMD apachectl -k start -X
```

## EJEMPLO DE Dockerfile

```
# ldapserver
# slapd es el demonio
# ldap-utils son herramientas

FROM debian
LABEL subject="ldap server 2022"
LABEL author="Isaac"
ARG DEBIAN_FRONTEND=noninteractive
RUN apt-get update && apt-get -y install procps tree nmap vim iproute2 iputils-ping slapd ldap-utils
RUN mkdir /opt/docker
WORKDIR /opt/docker
COPY * /opt/docker/
RUN chmod +x /opt/docker/startup.sh
CMD /opt/docker/startup.sh
EXPOSE 389
```

Una vez creado el Dockerfile, se tiene que crear el script que este ejecutará al iniciar el contenedor. Igual que antes, hay un ejemplo de este archivo tras la siguiente explicación.

```
vim startup.sh
```

## EXPLICACIÓN DE LO QUE CONTIENE startup.sh

- shebang.

```
#! /bin/bash
```

- Primero hay que borrar los datos que contiene por defecto la base de datos. IMPORTANTE: añadir **'*'** al final para no borrar la carpeta.

```
rm -rf /var/lib/ldap/*
```

- Tras esto hay que hacer lo mismo con la carpeta de configuración.

```
rm -rf /etc/ldap/slapd.d/*
```

- A partir del archivo **sladp.conf** de la carpeta creada al inicio del documento, verificar si este está bien escrito (-f) y bien configurado (-F) y generar una configuración dinámica.

```
slaptest -f slapd.conf -F /etc/ldap/slapd.d
```

- A partir del archivo **.ldif** (-l) de la carpeta anteriormente mencionada, hacer la carga de datos inicial de forma masiva (popular) a bajo nivel, es decir, con el demonio parado y accediendo a los archivos de la base de datos, un archivo de configuración (-F) dentro de la base de datos.

```
slapadd -F /etc/ldap/slapd.d/ -l edt-org.ldif
```

- Establecer el usuario **openldap.openldap** como propietario de forma recursiva (-R) las carpetas donde se han realizado los anteriores cambios.

```
chown -R openldap.openldap /etc/ldap/slapd.d/ /var/lib/ldap/
```

- Encender el demonio en foreground. Esto lo hace gracias a estar en modo debug (-d).

```
/usr/sbin/slapd -d0
```

Una vez ejecutado este script, se debe comprobar que el demonio esté encendido con **ps ax**, ver qué dirección IP tiene el contenedor con **nmap localhost**.

## EJEMPLO DE startup.sh

```
#! /bin/bash

rm -rf /var/lib/ldap/*
rm -rf /etc/ldap/slapd.d/*

slaptest -f slapd.conf -F /etc/ldap/slapd.d
slapadd -F /etc/ldap/slapd.d/ -l edt-org.ldif
chown -R openldap.openldap /etc/ldap/slapd.d/ /var/lib/ldap/

/usr/sbin/slapd -d0
```

## EJECUCIÓN DEL CONTAINER

Al haber concluido con la configuración del container solo queda ejecutarlo. Para esto primero hay que crear una imagen del container estando en la carpeta creada al inicio.

```
docker build -t isaacgm22/ldapserver:server_base_2022 .
```

- Además, se crea una red. Si esta es la primera creada (normalmente hay 3 por defecto) su dirección IP será **172.18.0.0/16**, donde el contenedor que se añada tendrá como IP **172.18.0.2**.

```
docker network create 2hisx
```

- Se comprueba que se ha creado correctamente.

```
docker network ls
```

- `Finalmente ya se puede crear un container que se autodestruye (--rm), de nombre **ldap.edt.org** (--name) con host **ldap.edt.org** (-h), que pertenezca a la red creada (--network) en detach (-d).`


```
docker run --rm --name ldap.edt.org -h ldap.edt.org --network 2hisx -d isaacgm22/ldapserver:server_base_2022
```

## COMPROBACIÓN

- Verificar que el contenedor está en ejecución en segundo plano.

```
docker ps
```

- Verificar que el demonio está funcionando en segundo plano dento del contenedor.

```
docker exec -it ldap.edt.org ps ax
```

- Verificar la dirección IP del contenedor.

```
docker exec -it ldap.edt.org ip a
```

- Verificar que el puerto del **ldap (389)**, está abierto.

```
docker exec -it ldap.edt.org nmap localhost
```

Se puede comprobar también desde fuera del contenedor.

```
nmap 172.18.0.2
```

- Verificar que existe la base de datos que se ha creado y que está está poblada. Lo que hace esta orden es hacer una búsqueda con ldap en el host indicado (-h), sin usar ningún método de autenticación (-x), creando una lista muy resumida (-LLL -cuantas más Ls, menos información-) y buscando en la base de datos (-b) estableciendo como base del árbol de búsqueda 'dc=edt,dc=org' aquellos contenidos que sean **DNs** o **distinguished names**.

```
ldapsearch -h 172.17.0.2 -x -LLL -b 'dc=edt,dc=org' dn
```

Cuando se ejecuta esta orden aparece lo siguiente:

```
dn: dc=edt,dc=org

dn: ou=maquines,dc=edt,dc=org

dn: ou=clients,dc=edt,dc=org

dn: ou=productes,dc=edt,dc=org

dn: ou=usuaris,dc=edt,dc=org

dn: cn=Pau Pou,ou=usuaris,dc=edt,dc=org

dn: cn=Pere Pou,ou=usuaris,dc=edt,dc=org

dn: cn=Anna Pou,ou=usuaris,dc=edt,dc=org

dn: cn=Marta Mas,ou=usuaris,dc=edt,dc=org

dn: cn=Jordi Mas,ou=usuaris,dc=edt,dc=org

dn: cn=Admin System,ou=usuaris,dc=edt,dc=org
```

- Si se quiere entrar al contenedor de forma interactiva se puede usar esta orden:

```
docker exec -it ldap.edt.org /bin/bash
```

Esto es de utilidad si no se tienen instalados algunos comandos de ldap fuera del contenedor. Uno que se puede usar es **slapcat**, que muestra lo que tiene la base de datos:

```
dn: dc=edt,dc=org
dc: edt
description: Escola del treball de Barcelona
objectClass: dcObject
objectClass: organization
o: edt.org
structuralObjectClass: organization
entryUUID: cb5ced20-cc4d-103c-8851-c3051f087995
creatorsName: cn=Manager,dc=edt,dc=org
createTimestamp: 20220919100137Z
entryCSN: 20220919100137.405170Z#000000#000#000000
modifiersName: cn=Manager,dc=edt,dc=org
modifyTimestamp: 20220919100137Z
```

Esto es un **DIT** o **Data Information Tree**.

El primer elemento de un ldif (base de datos) es el **nódulo raíz**, en este caso es una organización llamada 'dc=edt,dc=org'. Dentro de esta hay entidades, que son 'maquines', 'clients', 'productes' y 'usuaris'. En este caso se ha populado la entidad de usuarios, cada uno con sus datos.

Este primer bloque de datos contiene el nombre (dn) de la organización, su descripción (opcional), qué tipo de entidad es dentro de esta estructura de datos (objectClass: organization) y qué nombre tiene la organización (o: edt.org).

```
dn: ou=maquines,dc=edt,dc=org
ou: maquines
description: Container per a maquines linux
objectClass: organizationalunit
structuralObjectClass: organizationalUnit
entryUUID: cb5d5e54-cc4d-103c-8852-c3051f087995
creatorsName: cn=Manager,dc=edt,dc=org
createTimestamp: 20220919100137Z
entryCSN: 20220919100137.408125Z#000000#000#000000
modifiersName: cn=Manager,dc=edt,dc=org
modifyTimestamp: 20220919100137Z
```

Este otro bloque de datos tiene una estructura similar al anterior. La diferencia importante es que esta entidad llamada 'maquines' es de tipo **objectClass: organizationalunit**.