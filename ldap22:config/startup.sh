#! /bin/bash

mkdir /var/lib/ldap-isaac
rm -rf /var/lib/ldap-isaac/*
rm -rf /etc/ldap/slapd.d/*

slaptest -f slapd.conf -F /etc/ldap/slapd.d
slapadd -F /etc/ldap/slapd.d/ -l edt-org.ldif
slapadd -n2 -F /etc/ldap/slapd.d/ -l isaac-cat.ldif
chown -R openldap.openldap /etc/ldap/slapd.d/ /var/lib/ldap-isaac

/usr/sbin/slapd -d0
