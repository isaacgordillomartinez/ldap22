# LDAP SERVER 2022 EN DETACH
# Isaac Gordillo


Este es un container de Docker para practicar modificaciones de la base de datos.

**Archivos que no cambian respecto al container base:**

- Dockerfile
- startup.sh

## CAMBIOS EN EL ARCHIVO edt-org.ldif

- Todos los usuarios dejan de identificarse con el campo **common name (cn)** para hacerlo con el de **uid**.

  ```
  dn: uid=pau,ou=usuaris,dc=edt,dc=org            # Aquí está el cambio.
  objectclass: posixAccount
  objectclass: inetOrgPerson
  cn: Pau Pou
  cn: Pauet Pou
  sn: Pou
  homephone: 555-222-2220
  mail: pau@edt.org
  description: Watch out for this guy
  ou: Profes
  uid: pau
  uidNumber: 5000
  gidNumber: 100
  homeDirectory: /tmp/home/pau
  userPassword: {SSHA}NDkipesNQqTFDgGJfyraLz/csZAIlk2/
  ```

- Ahora existen nuevos usuarios **(usuari0-11)** pertenecientes a la **ou=usuaris**.
  * Todos tienen como contraseña la cadena **'jupiter'** encriptada usando el protocolo **SSHA**.

    ```
    dn: uid=usuari0,ou=usuaris,dc=edt,dc=org
    objectclass: posixAccount
    objectclass: inetOrgPerson
    cn: usuari0
    cn: 0
    sn: 0
    homephone: 555-222-2224
    mail: usuari0@edt.org
    description: This username is very original.
    ou: Usuaris
    ou: Profes
    uid: usuari0
    uidNumber: 0000
    gidNumber: 100
    homeDirectory: /tmp/home/usuari0
    userPassword: {SSHA}KbzAf/nGTXMKHhZ3zf0EWOSN3LuClDIq      # Esto significa 'jupiter'.
    ```

  * Para crearla, se puede ejecutar la orden de servidor **slappasswd** e introducir una cadena de carácteres. Su output es un hash encriptado equivalente a esta. Dicha orden permite usar otros métodos de encriptación como **md5** y **CRYPT**.

    ```
    slappasswd -h {md5}
    slappasswd -h {CRYPT}
    ```

- También se ha creado una nueva organizational unit. En este caso es **campeones**.

    ```
    dn: ou=campeones,dc=edt,dc=org
    ou: campeones
    description: Seccion de campeones.
    objectclass: organizationalunit
    ```

  * Esta ou contiene dos usuarios: **Amumu** y **Yorick**.

    ```
    dn: uid=amumu,ou=campeones,dc=edt,dc=org
    objectClass: posixAccount
    objectClass: inetOrgPerson
    cn: Amumu
    cn: Emumu
    cn: Jungler
    sn:: VG9udMOtc2ltbw==
    homePhone: 555-222-2222
    mail: depresion@plata3.lol
    description: Este campeon no hace mas que llorar.
    ou: campeones
    uid: amumu
    uidNumber: 69420
    gidNumber: 200
    homeDirectory: /tmp/home/jungler1
    userPassword: {SSHA}KbzAf/nGTXMKHhZ3zf0EWOSN3LuClDIq
    ```

    ```
    dn: uid=yorick,ou=campeones,dc=edt,dc=org
    objectClass: posixAccount
    objectClass: inetOrgPerson
    cn: Yorick
    cn: Rompetechos
    cn: Toplaner
    sn:: VG9udMOtc2ltbw==
    homePhone: 555-222-2223
    mail: basura@bronce1.lol
    description: Este campeon rompe todo lo que encuentra con su pala.
    ou: campeones
    uid: yorick
    uidNumber: 69421
    gidNumber: 200
    homeDirectory: /tmp/home/toplaner1
    userPassword: {SSHA}KbzAf/nGTXMKHhZ3zf0EWOSN3LuClDIq
    ```

- A parte de estos archivos, dentro del contenedor hay archivos **.ldif** para cambiar la configuración del servidor.

  * Para cambiar la contraseña del usuario administrador **(cn=Manager)** de la base de datos **dc=edt,dc=org** está el archivo **modManager.ldif**, que se usará más adelante.

    ```
    # La contraseña de Manager (olcRootPW es 'secret').
    dn: olcDatabase={1}mdb,cn=config
    changetype: modify
    replace: olcRootPW
    olcRootPW: {SSHA}Ss5XDyUQUSseMFJWqLgRNXLIxfskyWPF
    ```

## CAMBIOS EN EL ARCHIVO slapd.conf

Si se quiere cambiar, por ejemplo, la contraseña del administrador **(cn=Manager)** de la base de datos **dc=edt,dc=org**, solo se puede hacer en frío, es decir, se tiene que parar la ejecución del contenedor y modificar el campo **rootpw** del archivo **slapd.conf**. Tras esto, es necesario crear de nuevo el container (docker build) y ejecutarlo (docker run). 

- El primer cambio que se ha hecho es **encriptar la contraseña con SSHA** del usuario **Manager**.

- Como apunte, este archivo permite por defecto el acceso **(access to * )** y la lectura **(by * read)** de todo el contenido a todos los usuarios de la base de datos, pero solo modificar los campos de un usuario al que ha accedido.

  ```
  database mdb
  suffix "dc=edt,dc=org"
  rootdn "cn=Manager,dc=edt,dc=org"
  rootpw {SSHA}Z6OL04reE18latZ+7qt6cI8kwZHanKgX             # Esta es la contraseña encriptada.
  directory /var/lib/ldap
  index objectClass eq,pres
  access to * by self write by * read                       # Aquí están los permisos.
  # el passwd es secret
  ```

  * Por ejemplo: solo el usuario **uid=pere,dc=edt,dc=org** puede modificar sus datos (además de **cn=Manager,dc=edt,dc=org**).

  * Usuario Manager modificando la contraseña del usuario Pere Pou a **'pere'**:

    ```
    ldapmodify -xv -D 'cn=Manager,dc=edt,dc=org' -w secret -f modPere.ldif
    ```

    ```
    replace userPassword:
            pere
    modifying entry "uid=pere,ou=usuaris,dc=edt,dc=org"
    modify complete
    ```

    * Contenido de **modPere.ldif**.

      ```
      dn: uid=pere,ou=usuaris,dc=edt,dc=org
      changetype: modify
      replace: userPassword
      userPassword: pere
      ```

  * Usuario Pere Pou modificando su propia contraseña a **'jupiter'**:

      ```
      ldapmodify -xv -D 'cn=Manager,dc=edt,dc=org' -w secret -f modPere.ldif
      ```

      ```
      replace userPassword:
              jupiter
      modifying entry "uid=pere,ou=usuaris,dc=edt,dc=org"
      modify complete
      ```

    * Nuevo contenido de **modPere.ldif**.

      ```
      dn: uid=pere,ou=usuaris,dc=edt,dc=org
      changetype: modify
      replace: userPassword
      userPassword: jupiter
      ```

- Siguiendo con los cambios respecto a la versión anterior de este archivo, se ha añadido una nueva base de datos con el usuario administrador de todas las BDs **(rootdn cn=Sysadmin,cn=config)**. En realidad la base de datos ya existe de forma predeterminada en el servidor ldap, solo que se le ha añadido un usuario administrador con su contraseña para poder realizar cambios en la base de datos de configuración en caliente, es decir, sin tener que parar la ejecución del contenedor, tal y como ha quedado explicado anteriormente.

  * Esto es lo que se ha añadido en el archivo **slapd.conf** justo después de la primera base de datos:

    ```
    # ----------------------------------------------------------------------
    database config
    rootdn "cn=Sysadmin,cn=config"
    rootpw {SSHA}vwpQxtzc7yLsGg8K7fm02p2Fhox/PFP4
    # el passwd es syskey
    # ----------------------------------------------------------------------
    ```

  * Así es cómo se veía antes la BD **config** usando la orden de servidor **slapcat -n0**:

    ```
    dn: olcDatabase={0}config,cn=config
    objectClass: olcDatabaseConfig
    olcDatabase: {0}config
    olcAccess: {0}to *  by * none
    olcAddContentAcl: TRUE
    olcLastMod: TRUE
    olcMaxDerefDepth: 15
    olcReadOnly: FALSE
    olcRootDN: cn=config                                 # Nótese que en esta línea el usuario
    olcSyncUseSubentry: FALSE                            # el usuario por defecto es cn=config.
    olcMonitoring: FALSE
    structuralObjectClass: olcDatabaseConfig
    entryUUID: ebe696c4-da01-103c-9979-03a709477693
    creatorsName: cn=config
    createTimestamp: 20221006203346Z
    entryCSN: 20221006203346.518713Z#000000#000#000000
    modifiersName: cn=config
    modifyTimestamp: 20221006203346Z
    ```
  
  * Así es cómo se ve ahora la BD config tras los cambios:

    ```
    dn: olcDatabase={0}config,cn=config
    objectClass: olcDatabaseConfig
    olcDatabase: {0}config
    olcAccess: {0}to *  by * none
    olcAddContentAcl: TRUE
    olcLastMod: TRUE
    olcMaxDerefDepth: 15
    olcReadOnly: FALSE
    olcRootDN: cn=Sysadmin,cn=config                                    # Ahora el usuario es Sysadmin.
    olcRootPW:: e1NTSEF9dndwUXh0emM3eUxzR2c4SzdmbTAycDJGaG94L1BGUDQ=    # Esta es su contraseña encriptada.
    olcSyncUseSubentry: FALSE
    olcMonitoring: FALSE
    structuralObjectClass: olcDatabaseConfig
    entryUUID: 5fb6b634-da01-103c-8e2f-73497b187b66
    creatorsName: cn=config
    createTimestamp: 20221006202951Z
    entryCSN: 20221006202951.324351Z#000000#000#000000
    modifiersName: cn=config
    modifyTimestamp: 20221006202951Z
    ```

## MODIFICACIÓN DE LA CONFIGURACIÓN DE LA BASE DE DATOS EN CALIENTE

Al haber realizado estos cambios en los archivos del contenedor, ahora se puede comprobar que se puede modificar, por ejemplo, la contraseña del administrador **(cn=Manager)** de la base de datos **dc=edt,dc=org** en caliente.

- Para hacer esto se usa el archivo **modManager.ldif** del contenedor. **IMPORTANTE**: en la base de datos de configuración los campos tienen una **sintaxis de domain name (dn) diferente**.

  ```
  # La contraseña de Manager (olcRootPW es 'secret').
  dn: olcDatabase={1}mdb,cn=config                        # Aquí está la diferencia.
  changetype: modify
  replace: olcRootPW
  olcRootPW: {SSHA}Z6OL04reE18latZ+7qt6cI8kwZHanKgX
  ```

- Luego realiza el cambio mediante el usuario administrador **(cn=Sysadmin)** de la BD **cn=config**.

  ```
  ldapmodify -xv -D 'cn=Sysadmin,cn=config' -w syskey -f modManager.ldif

  ldap_initialize( <DEFAULT> )
  replace olcRootPW:
          {SSHA}Ss5XDyUQUSseMFJWqLgRNXLIxfskyWPF
  modifying entry "olcDatabase={1}mdb,cn=config"
  modify complete
  ```

- COMPROBACIÓN:

  ```
  ldapwhoami -xv -D 'cn=Manager,dc=edt,dc=org' -w secret

  ldap_initialize( <DEFAULT> )
  dn:cn=Manager,dc=edt,dc=org
  Result: Success (0)
  ```

## EJECUCIÓN DEL CONTAINER

```
docker build -t isaacgm22/ldap22:editat .
```

```
docker run --rm --name ldap.edt.org -h ldap.edt.org --network 2hisx -p 389:389 -d isaacgm22/ldap22:editat
```

- IMPORTANTE: Usando la opción **-p** se escoge a qué rango de puertos del **host** se quiere propagar un cierto rango de puertos expuestos del **container**. Si se usase la opción **-P=true** se propagarían los puertos expuestos del container a unos **aleatorios** del host. Al haber propagado el puerto no es necesario añadir la opción **-h** con la dirección IP del contenedor, ya que por defecto se usa **-h localhost**.

```
-p hostPort:containerPort
```