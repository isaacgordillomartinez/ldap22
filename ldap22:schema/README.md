# LDAP SERVER 2022 EN DETACH
# Isaac Gordillo


Este es un container de Docker para crear una base de datos sobre futbolistas.

**Archivos que no cambian respecto al container base:**

- Dockerfile
- startup.sh

```
          WHAT                  WHO    ACCESS
access to attrs=homePhone  by   *      read
                           by   *      write
```

## FURBO.SCHEMA

TODA ENTIDAD HA DE TENER UN SOLO OBJETO ESTRUCTURAL PERO PUEDE TENER VARIOS OBJETOS AUXILIARES.

POR EJEMPLO: inetOrgPerson es la esctuctura

## EJECUCIÓN DEL CONTAINER

```
docker build -t isaacgm22/ldap22:schema .
```

```
docker run --rm --name ldap.edt.org -h ldap.edt.org --network 2hisx -p 389:389 -d isaacgm22/ldap22:schema
```
