# LDAP SERVER 2022 EN DETACH
# Isaac Gordillo


Este es un container de Docker para hacer pruebas de ACL.

**Archivos que no cambian respecto al container base:**

- Dockerfile
- startup.sh

```
          WHAT                  WHO    ACCESS
access to attrs=homePhone  by   *      read
                           by   *      write
```

## EJECUCIÓN DEL CONTAINER

```
docker build -t isaacgm22/ldap22:acl .
```

```
docker run --rm --name ldap.edt.org -h ldap.edt.org --network 2hisx -p 389:389 -d isaacgm22/ldap22:acl
```

## EJEMPLO 1

Cambiar los permisos de la base de datos 1 con tal que los usuarios solo tengan permisos de lectura. **(dc=edt,dc=org)**.

- Para esto hace falta ejecutar la siguiente orden usando el usuario administrador de todas las BDs **(cn=Sysadmin,cn=config)**.

  ```
  ldapmodify -xv -D 'cn=Sysadmin,cn=config' -w syskey -f ejemplos/aclEjemplo1.ldif
  ```

  ```
  ldap_initialize( <DEFAULT> )
  delete olcAccess:
  add olcAccess:
          to * by * read
  modifying entry "olcDatabase={1}mdb,cn=config"
  modify complete
  ```

  * Contenido del archivo **aclEjemplo1.ldif**.

    ```
    dn: olcDatabase={1}mdb,cn=config
    changetype: modify
    delete: olcAccess
    -
    add: olcAccess
    olcAccess: to * by * read
    ```

  * COMPROBACIÓN 1. Los permisos se han cambiado correctamente:

    ```
    ldapsearch -xv -LLL -D 'cn=Sysadmin,cn=config' -w syskey -b 'olcDatabase={1}mdb,cn=config' olcAccess
    ```

    ```
    ldap_initialize( <DEFAULT> )
    filter: (objectclass=*)
    requesting: olcAccess 
    dn: olcDatabase={1}mdb,cn=config
    olcAccess: {0}to * by * read
    ```

  * COMPROBACIÓN 2. Los usuarios **solo pueden leer la base de datos**.

    ```
    ldapsearch -xv -LLL -D 'uid=pere,ou=usuaris,dc=edt,dc=org' -w pere -b 'dc=edt,dc=org' | grep dn
    ```

    ```
    ldap_initialize( <DEFAULT> )
    filter: (objectclass=*)
    requesting: All userApplication attributes
    dn: dc=edt,dc=org
    dn: ou=maquines,dc=edt,dc=org
    dn: ou=clients,dc=edt,dc=org
    dn: ou=productes,dc=edt,dc=org
    dn: ou=usuaris,dc=edt,dc=org
    dn: uid=pau,ou=usuaris,dc=edt,dc=org
    dn: uid=pere,ou=usuaris,dc=edt,dc=org
    dn: uid=anna,ou=usuaris,dc=edt,dc=org
    dn: uid=marta,ou=usuaris,dc=edt,dc=org
    dn: uid=jordi,ou=usuaris,dc=edt,dc=org
    dn: uid=admin,ou=usuaris,dc=edt,dc=org
    dn: uid=usuari0,ou=usuaris,dc=edt,dc=org
    dn: uid=usuari1,ou=usuaris,dc=edt,dc=org
    dn: uid=usuari2,ou=usuaris,dc=edt,dc=org
    dn: uid=usuari3,ou=usuaris,dc=edt,dc=org
    dn: uid=usuari4,ou=usuaris,dc=edt,dc=org
    dn: uid=usuari5,ou=usuaris,dc=edt,dc=org
    dn: uid=usuari6,ou=usuaris,dc=edt,dc=org
    dn: uid=usuari7,ou=usuaris,dc=edt,dc=org
    dn: uid=usuari8,ou=usuaris,dc=edt,dc=org
    dn: uid=usuari9,ou=usuaris,dc=edt,dc=org
    dn: uid=usuari10,ou=usuaris,dc=edt,dc=org
    dn: uid=usuari11,ou=usuaris,dc=edt,dc=org
    dn: ou=campeones,dc=edt,dc=org
    dn: uid=amumu,ou=campeones,dc=edt,dc=org
    dn: uid=yorick,ou=campeones,dc=edt,dc=org
    ```

## EJEMPLO 2

Los usuarios pueden hacer de todo (salvo órdenes exclusivas de root).

  ```
  ldapmodify -xv -D 'cn=Sysadmin,cn=config' -w syskey -f ejemplos/aclEjemplo2.ldif
  ```

  ```
  dn: olcDatabase={1}mdb,cn=config
  changetype: modify
  delete: olcAccess
  -
  add: olcAccess
  olcAccess: to * by * write
  ```

  - COMPROBACIÓN 1. Los permisos se han cambiado correctamente:

    ```
    ldapsearch -xv -LLL -D 'cn=Sysadmin,cn=config' -w syskey -b 'olcDatabase={1}mdb,cn=config' olcAccess
    ```

    ```
    ldap_initialize( <DEFAULT> )
    filter: (objectclass=*)
    requesting: olcAccess 
    dn: olcDatabase={1}mdb,cn=config
    olcAccess: {0}to * by * write
    ```

  - COMPROBACIÓN 2. Los usuarios **pueden hacer de todo**.

    * Pere puede ver los datos de Marta.

      ```
      ldapsearch -xv -LLL -D 'uid=pere,ou=usuaris,dc=edt,dc=org' -w pere -b 'dc=edt,dc=org' uid=marta
      ```

      ```
      ldap_initialize( <DEFAULT> )
      filter: uid=marta
      requesting: All userApplication attributes
      dn: uid=marta,ou=usuaris,dc=edt,dc=org
      objectClass: posixAccount
      objectClass: inetOrgPerson
      cn: Marta Mas
      sn: Mas
      homePhone: 555-222-2223
      mail: marta@edt.org
      description: Watch out for this girl
      ou: Alumnes
      uid: marta
      uidNumber: 5003
      gidNumber: 600
      homeDirectory: /tmp/home/marta
      userPassword:: e1NTSEF9OSsxRjJmNXZjVzh6L3RtU3pZTldkbHo1R2JEQ3lvT3c=
      ```

    * Pere puede modificar los atributos de Marta.

      ```
      ldapmodify -D 'uid=pere,ou=usuaris,dc=edt,dc=org' -w pere -f ejemplos/aclEjemplo2pere.ldif
      ```

      ```
      modifying entry "uid=marta,ou=usuaris,dc=edt,dc=org"
      ```

    * Contenido del archivo **aclEjemplo2pere.ldif**.

      ```
      dn: uid=marta,ou=usuaris,dc=edt,dc=org
      changetype: modify
      replace: mail
      mail: perehacker@edt.org
      ```

      ```
      ldap_initialize( <DEFAULT> )
      filter: uid=marta
      requesting: All userApplication attributes
      dn: uid=marta,ou=usuaris,dc=edt,dc=org
      objectClass: posixAccount
      objectClass: inetOrgPerson
      cn: Marta Mas
      sn: Mas
      homePhone: 555-222-2223
      description: Watch out for this girl
      ou: Alumnes
      uid: marta
      uidNumber: 5003
      gidNumber: 600
      homeDirectory: /tmp/home/marta
      userPassword:: e1NTSEF9OSsxRjJmNXZjVzh6L3RtU3pZTldkbHo1R2JEQ3lvT3c=
      mail: perehacker@edt.org                          # Aquí está el cambio. El atributo pasa a estar en última fila ya sea usando replace o delete en el .ldif
      ```

## EJEMPLO 3

Los usuarios pueden modificar sus propios datos pero solo pueden leer los de los demás.

  ```
  ldapmodify -xv -D 'cn=Sysadmin,cn=config' -w syskey -f ejemplos/aclEjemplo3.ldif
  ```

  ```
  dn: olcDatabase={1}mdb,cn=config
  changetype: modify
  delete: olcAccess
  -
  add: olcAccess
  olcAccess: to * by self write by * read
  ```

  - COMPROBACIÓN 1. Los permisos se han cambiado correctamente:

    ```
    ldapsearch -xv -LLL -D 'cn=Sysadmin,cn=config' -w syskey -b 'olcDatabase={1}mdb,cn=config' olcAccess
    ```

    ```
    ldap_initialize( <DEFAULT> )
    filter: (objectclass=*)
    requesting: olcAccess 
    dn: olcDatabase={1}mdb,cn=config
    olcAccess: {0}to * by self by * read
    ```

  - COMPROBACIÓN 2. Los usuarios **pueden modificar sus propios datos pero solo pueden leer los de los demás**.

    * Pere puede ver los datos de Marta.

      ```
      ldapsearch -xv -LLL -D 'uid=pere,ou=usuaris,dc=edt,dc=org' -w pere -b 'dc=edt,dc=org' uid=marta
      ldap_initialize( <DEFAULT> )
      filter: uid=marta
      requesting: All userApplication attributes
      dn: uid=marta,ou=usuaris,dc=edt,dc=org
      objectClass: posixAccount
      objectClass: inetOrgPerson
      cn: Marta Mas
      sn: Mas
      homePhone: 555-222-2223
      description: Watch out for this girl
      ou: Alumnes
      uid: marta
      uidNumber: 5003
      gidNumber: 600
      homeDirectory: /tmp/home/marta
      userPassword:: e1NTSEF9OSsxRjJmNXZjVzh6L3RtU3pZTldkbHo1R2JEQ3lvT3c=
      mail: perehacker@edt.org
      ```

    * Pere no puede modificar los atributos de Marta.
      Prueba con el archivo del ejemplo anterior:

      ```
      ldapmodify -xv -D 'uid=pere,ou=usuaris,dc=edt,dc=org' -w pere -f ejemplos/aclEjemplo2pere.ldif
      ```

      ```
      ldap_initialize( <DEFAULT> )
      replace mail:
        perehacker@edt.org
      modifying entry "uid=marta,ou=usuaris,dc=edt,dc=org"
      ldap_modify: Insufficient access (50)                # No hay suficiente acceso.
      ```

    * Marta puede modificar sus propios datos.
      Prueba con el archivo aclEjemplo3marta.ldif:

      ```
      ldap_initialize( <DEFAULT> )
      replace mail:
        marta@edt.org
      modifying entry "uid=marta,ou=usuaris,dc=edt,dc=org"
      modify complete
      ```





## EJEMPLO 4

Los usuarios pueden ver todos teléfonos y modificar todos los datos.
Escribir estos permisos es algo inútil, dado que cuando se haga una consulta filtrando por el campo del teléfono se aplicará la primera regla y no se continuarán evaluando el resto. Sin embargo, si se está filtrando por otro campo se aplicará la segunda regla y dará permisos superiores.

  ```
  ldapmodify -xv -D 'cn=Sysadmin,cn=config' -w syskey -f ejemplos/aclEjemplo4.ldif
  ```

  ```
  dn: olcDatabase={1}mdb,cn=config
  changetype: modify
  delete: olcAccess
  -
  add: olcAccess
  olcAccess: to attrs=homePhone by * read
  -
  add: olcAccess
  olcAccess: to * by * write
  ```

  - COMPROBACIÓN 1. Los permisos se han cambiado correctamente:

    ```
    ldapsearch -xv -LLL -D 'cn=Sysadmin,cn=config' -w syskey -b 'olcDatabase={1}mdb,cn=config' olcAccess
    ```

    ```
    ldap_initialize( <DEFAULT> )
    filter: (objectclass=*)
    requesting: olcAccess 
    dn: olcDatabase={1}mdb,cn=config
    olcAccess: {0}to attrs=homePhone by * read
    olcAccess: {1}to * by * write
    ```

  - COMPROBACIÓN 2. Se aplica la primera regla.
    Aparecen todos los teléfonos, pero para resumir aquí están los últimos dos.

    ```
    ldapsearch -xv -LLL -D 'uid=marta,ou=usuaris,dc=edt,dc=org' -w marta -b 'dc=edt,dc=org' homePhone
    ```

    ```
    dn: uid=amumu,ou=campeones,dc=edt,dc=org
    homePhone: 555-222-2222

    dn: uid=yorick,ou=campeones,dc=edt,dc=org
    homePhone: 555-222-2223
    ```

  - COMPROBACIÓN 3. Se aplica la segunda regla.

    ```
    ldapmodify -xv -D 'uid=pere,ou=usuaris,dc=edt,dc=org' -w pere -f ejemplos/aclEjemplo2pere.ldif
    ```

    ```
    ldap_initialize( <DEFAULT> )
    replace mail:
      perehacker@edt.org
    modifying entry "uid=marta,ou=usuaris,dc=edt,dc=org"
    modify complete
    ```

    * Pere, por ejemplo, puede volver a modificar los datos de Marta.

      ```
      ldapsearch -xv -LLL -D 'uid=pere,ou=usuaris,dc=edt,dc=org' -w pere -b 'dc=edt,dc=org' uid=marta
      ```

      ```
      ldap_initialize( <DEFAULT> )
      filter: uid=marta
      requesting: All userApplication attributes
      dn: uid=marta,ou=usuaris,dc=edt,dc=org
      objectClass: posixAccount
      objectClass: inetOrgPerson
      cn: Marta Mas
      sn: Mas
      homePhone: 555-222-2223
      description: Watch out for this girl
      ou: Alumnes
      uid: marta
      uidNumber: 5003
      gidNumber: 600
      homeDirectory: /tmp/home/marta
      userPassword:: e1NTSEF9OSsxRjJmNXZjVzh6L3RtU3pZTldkbHo1R2JEQ3lvT3c=
      mail: perehacker@edt.org
      ```

## EJEMPLO 5

El campo del teléfono de todos los usuarios solo puede ser modificado por Anna Pou y los demás usuarios solo lo pueden leer. A partir de esto, como la segunda regla implica a todos los otros campos, todos los usuarios podrán modificarlos.

  ```
  ldapmodify -xv -D 'cn=Sysadmin,cn=config' -w syskey -f ejemplos/aclEjemplo5.ldif
  ```

  ```
  dn: olcDatabase={1}mdb,cn=config
  changetype: modify
  delete: olcAccess
  -
  add: olcAccess
  olcAccess: to attrs=homePhone by * dn.exact="uid=anna,ou=usuaris,dc=edt,dc=org" write
                                by * read
  -
  add: olcAccess
  olcAccess: to * by * write
  ```

  - COMPROBACIÓN 1. Los permisos se han cambiado correctamente:

    ```
    ldapsearch -xv -LLL -D 'cn=Sysadmin,cn=config' -w syskey -b 'olcDatabase={1}mdb,cn=config' olcAccess
    ```

    ```
    ldap_initialize( <DEFAULT> )
    filter: (objectclass=*)
    requesting: olcAccess 
    dn: olcDatabase={1}mdb,cn=config
    olcAccess: {0}to attrs=homePhone by dn.exact="uid=anna,ou=usuaris,dc=edt,dc=or
    g" write                             by * read
    olcAccess: {1}to * by * write
    ```

  - COMPROBACIÓN 2. Anna Pou puede cambiar teléfonos ajenos.

    ```
    ldapmodify -xv -D 'uid=anna,ou=usuaris,dc=edt,dc=org' -w anna -f ejemplos/aclEjemplo5anna.ldif 
    ```

    ```
    ldap_initialize( <DEFAULT> )
    replace homePhone:
      123-123-123
    modifying entry "uid=marta,ou=usuaris,dc=edt,dc=org"
    modify complete
    ```

    * Los demás usuarios no pueden modificar este campo,

      ```
      ldapmodify -xv -D 'uid=jordi,ou=usuaris,dc=edt,dc=org' -w jordi -f ejemplos/aclEjemplo5anna.ldif
      ```

      ```
      ldap_initialize( <DEFAULT> )
      replace homePhone:
        123-123-123
      modifying entry "uid=marta,ou=usuaris,dc=edt,dc=org"
      ldap_modify: Insufficient access (50)
      ```

    * pero sí verlo.

      ```
      ldapsearch -xv -LLL -D 'uid=jordi,ou=usuaris,dc=edt,dc=org' -w jordi -b 'dc=edt,dc=org' uid=marta
      ```

      ```
      ldap_initialize( <DEFAULT> )
      filter: uid=marta
      requesting: All userApplication attributes
      dn: uid=marta,ou=usuaris,dc=edt,dc=org
      objectClass: posixAccount
      objectClass: inetOrgPerson
      cn: Marta Mas
      sn: Mas
      mail: marta@edt.org
      description: Watch out for this girl
      ou: Alumnes
      uid: marta
      uidNumber: 5003
      gidNumber: 600
      homeDirectory: /tmp/home/marta
      userPassword:: e1NTSEF9OSsxRjJmNXZjVzh6L3RtU3pZTldkbHo1R2JEQ3lvT3c=
      homePhone: 123-123-123
      ```

  - COMPROBACIÓN 3. Todos los usuarios pueden cambiar los otros campos.
                    Prueba con el archivo aclEjemplo2pere.ldif.

    ```
    ldapmodify -xv -D 'uid=jordi,ou=usuaris,dc=edt,dc=org' -w jordi -f ejemplos/aclEjemplo2pere.ldif 
    ```

    ```
    ldap_initialize( <DEFAULT> )
    replace mail:
      perehacker@edt.org
    modifying entry "uid=marta,ou=usuaris,dc=edt,dc=org"
    modify complete
    ```

## EJEMPLO 6

Los teléfonos de todos los usuarios pueden ser modificados por Anna Pou y el Admin System. Los demás usuarios solo los pueden leer.

Todo puede ser modificado por el Admin System, cada usuario puede modificar todos sus campos propios y todos pueden leer todos los campos.

**IMPORTANTE:** Al escribir los _by_ dentro del fichero .ldif solo se pueden usar espacios, no tabuladores.

  ```
  ldapmodify -xv -D 'cn=Sysadmin,cn=config' -w syskey -f ejemplos/aclEjemplo6.ldif
  ```

  ```
  dn: olcDatabase={1}mdb,cn=config
  changetype: modify
  delete: olcAccess
  -
  add: olcAccess
  olcAccess: to attrs=homePhone by dn.exact="uid=anna,ou=usuaris,dc=edt,dc=org" write
                                by dn.exact="uid=admin,ou=usuaris,dc=edt,dc=org" write
                                by * read
  -
  add: olcAccess
  olcAccess: to * by dn.exact="uid=admin,ou=usuaris,dc=edt,dc=org" write
                  by self write
                  by * read
  ```

  - COMPROBACIÓN 1. Los permisos se han cambiado correctamente:

    ```
    ldapsearch -xv -LLL -D 'cn=Sysadmin,cn=config' -w syskey -b 'olcDatabase={1}mdb,cn=config' olcAccess
    ```

    ```
    ldap_initialize( <DEFAULT> )
    filter: (objectclass=*)
    requesting: olcAccess 
    dn: olcDatabase={1}mdb,cn=config
    olcAccess: {0}to attrs=homePhone by dn.exact="uid=anna,ou=usuaris,dc=edt,dc=or
    g" write                             by dn.exact="uid=admin,ou=usuaris,dc=edt
    ,dc=org" write                             by * read
    olcAccess: {1}to * by dn.exact="uid=admin,ou=usuaris,dc=edt,dc=org" write     
              by self write               by * read
    ```

  - COMPROBACIÓN 2. El teléfono solo puede ser actualizado por Anna y Admin System.
                    Los demás usuarios solo pueden leer este campo o bien modificar
                    su propio teléfono.

    ```
    ldapmodify -xv -D 'uid=anna,ou=usuaris,dc=edt,dc=org' -w anna -f ejemplos/aclEjemplo5anna.ldif
    ```

    ```
    ldap_initialize( <DEFAULT> )
    replace homePhone:
      123-123-123
    modifying entry "uid=marta,ou=usuaris,dc=edt,dc=org"
    modify complete
    ```

## EJEMPLO 7

Todos los usuarios pueden modificar su propia contraseña (**by auth se usa para este campo**) y ver todos los campos salvo este de los demás usuarios.

  ```
  ldapmodify -xv -D 'cn=Sysadmin,cn=config' -w syskey -f ejemplos/aclEjemplo7.ldif
  ```

  ```
  dn: olcDatabase={1}mdb,cn=config
  changetype: modify
  delete: olcAccess
  -
  add: olcAccess
  olcAccess: to attrs=userPassword by self write
                                   by * auth
  -
  add: olcAccess
  olcAccess: to * by * read
  ```

  - COMPROBACIÓN 1. Los permisos se han cambiado correctamente:

    ```
    ldapsearch -xv -LLL -D 'cn=Sysadmin,cn=config' -w syskey -b 'olcDatabase={1}mdb,cn=config' olcAccess
    ```

    ```
    ldap_initialize( <DEFAULT> )
    filter: (objectclass=*)
    requesting: olcAccess 
    dn: olcDatabase={1}mdb,cn=config
    olcAccess: {0}to attrs=userPassword by self write                             
        by * auth
    olcAccess: {1}to * by * read
    ```

  - COMPROBACIÓN 2. Los usuarios pueden cambiar su propia contraseña

    ```
    ldapmodify -xv -D 'uid=pere,ou=usuaris,dc=edt,dc=org' -w pere -f ejemplos/aclEjemplo7passPere.ldif
    ```

    ```
    ldap_initialize( <DEFAULT> )
    replace userPassword:
      patata
    modifying entry "uid=pere,dc=edt,dc=org"
    ldap_modify: No such object (32)
      matched DN: dc=edt,dc=org
    ```

    y ver los otros atributos de los demás.

    ```
    ldapsearch -xv -LLL -D 'uid=jordi,ou=usuaris,dc=edt,dc=org' -w jordi -b 'dc=edt,dc=org' uid=pere
    ```

    ```
    ldap_initialize( <DEFAULT> )
    filter: uid=pere
    requesting: All userApplication attributes
    dn: uid=pere,ou=usuaris,dc=edt,dc=org
    objectClass: posixAccount
    objectClass: inetOrgPerson
    cn: Pere Pou
    sn: Pou
    homePhone: 555-222-2221
    mail: pere@edt.org
    description: Watch out for this guy
    ou: Profes
    uid: pere
    uidNumber: 5001
    gidNumber: 100
    homeDirectory: /tmp/home/pere
    ``` 

## EJERCICIO 1

**IMPORTANTE:** Escribir qué permisos tiene **root** es inútil dado que siempre tiene todos.

Anna Pou tiene permiso para modificarlo todo y todos pueden ver todos los datos.

  ```
  ldapmodify -xv -D 'cn=Sysadmin,cn=config' -w syskey -f aclEjercicio1.ldif
  ```

  ```
  dn: olcDatabase={1}mdb,cn=config
  changetype: modify
  delete: olcAccess
  -
  add: olcAccess
  olcAccess: to * by dn.exact="uid=anna,ou=usuaris,dc=edt,dc=org" write
  -
  add: olcAccess
  olcAccess: to * by * read
  ```

  - COMPROBACIÓN 1: Los cambios se han aplicado.

    ```
    ldapsearch -xv -LLL -D 'cn=Sysadmin,cn=config' -w syskey -b 'olcDatabase={1}mdb,cn=config' olcAccess
    ```

    ```
    ldap_initialize( <DEFAULT> )
    filter: (objectclass=*)
    requesting: olcAccess 
    dn: olcDatabase={1}mdb,cn=config
    olcAccess: {0}to * by dn.exact="uid=anna,ou=usuaris,dc=edt,dc=org" write
    olcAccess: {1}to * by * read
    ```

  - COMPROBACIÓN 2: Un usuario cualquiera puede ver datos de otro usuario.

    ```
    ldapsearch -vx -D "uid=pere,ou=usuaris,dc=edt,dc=org" -w pere -LLL -b "dc=edt,dc=org" uid="anna"
    ```

    ```
    ldap_initialize( <DEFAULT> )
    filter: uid=anna
    requesting: All userApplication attributes
    dn: uid=anna,ou=usuaris,dc=edt,dc=org
    objectClass: posixAccount
    objectClass: inetOrgPerson
    cn: Anna Pou
    cn: Anita Pou
    sn: Pou
    homePhone: 555-222-2222
    mail: anna@edt.org
    description: Watch out for this girl
    ou: Alumnes
    uid: anna
    uidNumber: 5002
    gidNumber: 600
    homeDirectory: /tmp/home/anna
    userPassword:: e1NTSEF9Qm00QjNCdS9mdUg2QmJ5OWxneGZGQXdMWXJLMFJiT3E=
    ```

## EJERCICIO 2

Anna Pou tiene permiso para modificarlo todo y todos pueden modificar su propio correo y teléfono. Todos pueden ver todos los datos.

**IMPORTANTE:** Hay que tener en cuenta qué ocurre con el resto de usuarios. Por defecto queda implícito que nadie puede hacer nada (**by * none**). Si se quiere cambiar esta condición, hay que explicitarlo en el archivo **slapd.conf** o en el **.ldif** usado para modificaciones.

  ```
  ldapmodify -xv -D 'cn=Sysadmin,cn=config' -w syskey -f aclEjercicio2.ldif
  ```

  ```
  dn: olcDatabase={1}mdb,cn=config
  changetype: modify
  delete: olcAccess
  -
  add: olcAccess
  olcAccess: to attrs=homePhone,mail by self write
  -
  add: olcAccess
  olcAccess: to * by dn.exact="uid=anna,ou=usuaris,dc=edt,dc=org" write
  ```

  - COMPROBACIÓN 1. Los permisos se han cambiado correctamente:

    ```
    ldapsearch -xv -LLL -D 'cn=Sysadmin,cn=config' -w syskey -b 'olcDatabase={1}mdb,cn=config' olcAccess
    ```

    ```

    ```

  - COMPROBACIÓN 2. Los usuarios **solo pueden leer la base de datos**.

    ```

    ```

    ```

    ```

## EJERCICIO 3

Todos los usuarios pueden modificar su propio correo. Todos pueden ver todos los datos.

  ```
  ldapmodify -xv -D 'cn=Sysadmin,cn=config' -w syskey -f aclEjercicio3.ldif
  ```

  ```
  dn: olcDatabase={1}mdb,cn=config
  changetype: modify
  delete: olcAccess
  -
  add: olcAccess
  olcAccess: to attrs=mail by self write
                           by * read
  -
  add: olcAccess
  olcAccess: 
  ```

  - COMPROBACIÓN 1. Los permisos se han cambiado correctamente:

    ```
    ldapsearch -xv -LLL -D 'cn=Sysadmin,cn=config' -w syskey -b 'olcDatabase={1}mdb,cn=config' olcAccess
    ```

    ```

    ```

  - COMPROBACIÓN 2. Los usuarios **solo pueden leer la base de datos**.

    ```

    ```

    ```

    ```

## EJERCICIO 4

Todos pueden ver todos los datos excepto los correos ajenos.

  ```
  ldapmodify -xv -D 'cn=Sysadmin,cn=config' -w syskey -f aclEjercicio4.ldif
  ```

  ```
  dn: olcDatabase={1}mdb,cn=config
  changetype: modify
  delete: olcAccess
  -
  add: olcAccess
  olcAccess: to attrs=mail by self read
                           by * none
  -
  add: olcAccess
  olcAccess: to 
  ```

  - COMPROBACIÓN 1. Los permisos se han cambiado correctamente:

    ```
    ldapsearch -xv -LLL -D 'cn=Sysadmin,cn=config' -w syskey -b 'olcDatabase={1}mdb,cn=config' olcAccess
    ```

    ```

    ```

  - COMPROBACIÓN 2. Los usuarios **solo pueden leer la base de datos**.

    ```

    ```

    ```

    ```

## EJERCICIO 5

Todos pueden modificar su propia contraseña y todos pueden ver todos los datos.

  ```
  ldapmodify -xv -D 'cn=Sysadmin,cn=config' -w syskey -f aclEjercicio5.ldif
  ```

  ```
  dn: olcDatabase={1}mdb,cn=config
  changetype: modify
  delete: olcAccess
  -
  add: olcAccess
  olcAccess: to attrs=passwd by self write
                             by * read
  olcAccess: to * by * read
  ```

  - COMPROBACIÓN 1. Los permisos se han cambiado correctamente:

    ```
    ldapsearch -xv -LLL -D 'cn=Sysadmin,cn=config' -w syskey -b 'olcDatabase={1}mdb,cn=config' olcAccess
    ```

    ```

    ```

  - COMPROBACIÓN 2. Los usuarios **solo pueden leer la base de datos**.

    ```

    ```

    ```

    ```

## EJERCICIO 6

Todos pueden modificar su propia contraseña y todos pueden ver solo sus propios datos.

  ```
  ldapmodify -xv -D 'cn=Sysadmin,cn=config' -w syskey -f aclEjercicio6.ldif
  ```

  ```
  dn: olcDatabase={1}mdb,cn=config
  changetype: modify
  delete: olcAccess
  -
  add: olcAccess
  olcAccess: to attrs=userPassword by self write
                                   by * auth
  olcAccess: to * by * read
  ```

  - COMPROBACIÓN 1. Los permisos se han cambiado correctamente:

    ```
    ldapsearch -xv -LLL -D 'cn=Sysadmin,cn=config' -w syskey -b 'olcDatabase={1}mdb,cn=config' olcAccess
    ```

    ```

    ```

  - COMPROBACIÓN 2. Los usuarios **solo pueden leer la base de datos**.

    ```

    ```

    ```

    ```

## EJERCICIO 7

Todos pueden modificar su propia contraseña y todos pueden ver todos los datos excepto las otras contraseñas.

  ```
  ldapmodify -xv -D 'cn=Sysadmin,cn=config' -w syskey -f aclEjercicio7.ldif
  ```

  ```
  dn: olcDatabase={1}mdb,cn=config
  changetype: modify
  delete: olcAccess
  -
  add: olcAccess
  olcAccess: to attrs=passwd by self write
                             by * read
  -
  add: olcAccess
  olcAccess: to * by * read
  ```

  - COMPROBACIÓN 1. Los permisos se han cambiado correctamente:

    ```
    ldapsearch -xv -LLL -D 'cn=Sysadmin,cn=config' -w syskey -b 'olcDatabase={1}mdb,cn=config' olcAccess
    ```

    ```

    ```

  - COMPROBACIÓN 2. Los usuarios **solo pueden leer la base de datos**.

    ```

    ```

    ```

    ```

## EJERCICIO 8

Todos pueden ver sus propios datos y modificar su propia contraseña, correo y teléfono. Anna Pou puede modificar todos los atributos excepto las contraseñas, que tampoco las puede ver, de todos usuarios. Pere Pou puede modificar las contraseñas de todos.

  ```
  ldapmodify -xv -D 'cn=Sysadmin,cn=config' -w syskey -f aclEjercicio8.ldif
  ```

  ```
  dn: olcDatabase={1}mdb,cn=config
  changetype: modify
  delete: olcAccess
  -
  add: olcAccess
  olcAccess: to attrs=userPassword,homePhone
             by self write
             by * read
  olcAccess: to attrs=mail
             by self write
             by * none
  ```

  - COMPROBACIÓN 1. Los permisos se han cambiado correctamente:

    ```
    ldapsearch -xv -LLL -D 'cn=Sysadmin,cn=config' -w syskey -b 'olcDatabase={1}mdb,cn=config' olcAccess
    ```

    ```

    ```

  - COMPROBACIÓN 2. Los usuarios **solo pueden leer la base de datos**.

    ```

    ```

    ```

    ```

## EJERCICIO 9

Todos los usuarios pueden modificar su contraseña, mail y teléfono, pero no pueden ver contraseñas ni teléfonos ajenos. El resto de atributos son visibles para todos.

  ```
  ldapmodify -xv -D 'cn=Sysadmin,cn=config' -w syskey -f aclEjercicio9.ldif
  ```

  ```
  dn: olcDatabase={1}mdb,cn=config
  changetype: modify
  delete: olcAccess
  -
  add: olcAccess
  olcAccess: to attrs=userPassword
             by self write
             by * auth
             by * none
  olcAccess: to attrs=mail
             by self write
             by * read
  olcAccess: to attrs=homePhone
             by self write
             by * none
  olcAccess: to *
             by * read
  ```

  - COMPROBACIÓN 1. Los permisos se han cambiado correctamente:

    ```
    ldapsearch -xv -LLL -D 'cn=Sysadmin,cn=config' -w syskey -b 'olcDatabase={1}mdb,cn=config' olcAccess
    ```

    ```

    ```

  - COMPROBACIÓN 2. Los usuarios **solo pueden leer la base de datos**.

    ```

    ```

    ```

    ```


## EJERCICIO 10

Anna Pou puede modificar todo. Pere Pou solo puede modificar el correo y el teléfono de todos. Todos los usuarios pueden ver todos los datos ajenos salvo las contraseñas.

  ```
  ldapmodify -xv -D 'cn=Sysadmin,cn=config' -w syskey -f aclEjercicio10.ldif
  ```

  ```
  dn: olcDatabase={1}mdb,cn=config
  changetype: modify
  delete: olcAccess
  -
  add: olcAccess
  olcAccess: to attrs=userPassword
             by exact.dn="uid=anna,ou=usuaris,dc=edt,dc=org" write
             by * auth            # Solo permite la autenticación, nada más. Ejemplo: ldapwhoami
  olcAccess: to attrs=mail,homePhone
             by exact.dn="uid=anna,ou=usuaris,dc=edt,dc=org" write
             by exact.dn="uid=pere,ou=usuaris,dc=edt,dc=org" write
             by * read
  olcAccess: to *
             by exact.dn="uid=anna,ou=usuaris,dc=edt,dc=org" write
             by * read
  ```

  - COMPROBACIÓN 1. Los permisos se han cambiado correctamente:

    ```
    ldapsearch -xv -LLL -D 'cn=Sysadmin,cn=config' -w syskey -b 'olcDatabase={1}mdb,cn=config' olcAccess
    ```

    ```

    ```

  - COMPROBACIÓN 2. Los usuarios **solo pueden leer la base de datos**.

    ```

    ```

    ```

    ```

## EJERCICIO 11

Anna Pou puede modificar todo. Pere Pou solo puede modificar el correo y el teléfono de todos. Todos los usuarios pueden ver todos los datos ajenos salvo las contraseñas y los teléfonos. Cada usuario puede ver su propia contraseña.

  ```
  ldapmodify -xv -D 'cn=Sysadmin,cn=config' -w syskey -f aclEjercicio10.ldif
  ```

  ```
  dn: olcDatabase={1}mdb,cn=config
  changetype: modify
  delete: olcAccess
  -
  add: olcAccess
  olcAccess: to attrs=userPassword
             by exact.dn="uid=anna,ou=usuaris,dc=edt,dc=org" write
             by self read
             by * auth            # Solo permite la autenticación, nada más. Ejemplo: ldapwhoami
  olcAccess: to attrs=mail
             by exact.dn="uid=anna,ou=usuaris,dc=edt,dc=org" write
             by exact.dn="uid=pere,ou=usuaris,dc=edt,dc=org" write
             by * read
  olcAccess: to attrs=homePhone
             by exact.dn="uid=anna,ou=usuaris,dc=edt,dc=org" write
             by exact.dn="uid=pere,ou=usuaris,dc=edt,dc=org" write
             by * none
  olcAccess: to *
             by exact.dn="uid=anna,ou=usuaris,dc=edt,dc=org" write
             by * read
  ```

  - COMPROBACIÓN 1. Los permisos se han cambiado correctamente:

    ```
    ldapsearch -xv -LLL -D 'cn=Sysadmin,cn=config' -w syskey -b 'olcDatabase={1}mdb,cn=config' olcAccess
    ```

    ```

    ```

  - COMPROBACIÓN 2. Los usuarios **solo pueden leer la base de datos**.

    ```

    ```

    ```

    ```