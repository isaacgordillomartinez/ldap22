# LDAP SERVER 2022 EN DETACH
# Isaac Gordillo

Este es un contenedor de Docker para crear clases de objetos relacionados con grupos.

## EJECUCIÓN DEL CONTENEDOR

```
docker build -t isaacgm22/ldap22:grups .
```

```
docker run --rm --name ldap.edt.org -h ldap.edt.org --network 2hisx -p 389:389 -p 636:636 -d isaacgm22/ldap22:grups
```

**Archivos que no cambian respecto al contenedor base:**

- startup.sh

## CAMBIOS EN EL ARCHIVO EDT-ORG.LDIF

- Se han modificado los **Relative Distinguished Names (RDN)** de los usuarios para que sean identificados mediante el atributo **uid** en vez de **cn**. Además, todos los usuarios tienen sus **gidNumbers** relacionados con las nuevas entidades. Como nota, el usuario 

  * Por ejemplo:

    ```
    dn: uid=pau,ou=usuaris,dc=edt,dc=org          # Aquí está el cambio.
    objectclass: posixAccount
    objectclass: inetOrgPerson
    cn: Pau Pou
    cn: Pauet Pou
    sn: Pou
    homephone: 555-222-2220
    mail: pau@edt.org
    description: Watch out for this guy
    ou: Profes
    uid: pau                                      # También aquí.
    uidNumber: 5000
    gidNumber: 601                                # Y aquí.
    homeDirectory: /tmp/home/pau
    userPassword: {SSHA}NDkipesNQqTFDgGJfyraLz/csZAIlk2/
    ```

- Se ha añadido una nueva **entidad** con **organizationalUnit** como **objectClass** llamada **grups**.

  ```
  dn: ou=grups,dc=edt,dc=org
  ou: grups
  description: Container para grupos.
  objectclass: organizationalunit
  ```

- Se han añadido las siguientes entidades con **posixGroup** como **objectClass**:

  * **IMPORTANTE**: posixGroup tiene como atributos obligatorios **cn** y **gidNumber**.
    * Por otro lado, tiene como opcionales **description**, **memberUid** y **userPassword**.

    * NOTA: El grupo de adminstración en Debian es **cn=sudoers** con **gidNumber: 27**, pero en Fedora es **cn=wheel** con **gidNumber: 10**.

      ```
      dn: cn=Alumnes,ou=grups,dc=edt,dc=org
      ou: grups
      description: Grupo de alumnos.
      gidNumber: 600
      memberUid: anna
      memberUid: marta
      objectClass: posixGroup
      ```

      ```
      dn: cn=Profes,ou=grups,dc=edt,dc=org
      ou: grups
      description: Grupo de profesores.
      gidNumber: 601
      memberUid: pau
      memberUid: pere
      memberUid: jordi
      objectClass: posixGroup
      ```

      ```
      dn: cn=1asix,ou=grups,dc=edt,dc=org
      ou: grups
      description: Grupo del primer curso de ASIX.
      gidNumber: 610
      memberUid: user01
      memberUid: user02
      memberUid: user03
      memberUid: user04
      memberUid: user05
      objectClass: posixGroup
      ```

      ```
      dn: cn=2asix,ou=grups,dc=edt,dc=org
      ou: grups
      description: Grupo del segundo curso de ASIX.
      gidNumber: 611
      memberUid: user06
      memberUid: user07
      memberUid: user08
      memberUid: user09
      memberUid: user10
      objectClass: posixGroup
      ```

      ```
      dn: cn=sudoers,ou=grups,dc=edt,dc=org
      ou: grups
      description: Grupo de sudoers.
      gidNumber: 27
      objectClass: posixGroup
      ```

      ```
      dn: cn=1wiam,ou=grups,dc=edt,dc=org
      ou: grups
      description: Grupo del primer curso de WIAM.
      gidNumber: 612
      objectClass: posixGroup
      ```

      ```
      dn: cn=2wiam,ou=grups,dc=edt,dc=org
      ou: grups
      description: Grupo del segundo curso de WIAM.
      gidNumber: 613
      objectClass: posixGroup
      ```

      ```
      dn: cn=1hiaw,ou=grups,dc=edt,dc=org
      ou: grups
      description: Grupo del primer curso de HIAW.
      gidNumber: 614
      objectClass: posixGroup
      ```

## CAMBIOS EN EL ARCHIVO SLAPD.CONF

Se han borrado todos aquellos schemas que no son utilizados. Aquí está la lista:

```
/etc/ldap/schema/corba.schema
/etc/ldap/schema/duaconf.schema
/etc/ldap/schema/dyngroup.schema
/etc/ldap/schema/java.schema              # Contenedores de java
/etc/ldap/schema/misc.schema
/etc/ldap/schema/openldap.schema
/etc/ldap/schema/ppolicy.schema           # Password policy
/etc/ldap/schema/collective.schema
```

Los que son esenciales para el funcionamiento del contenedor son los siguientes:

```
/etc/ldap/schema/core.schema              # Es el núcleo de todo
/etc/ldap/schema/cosine.schema            # Contiene dependencias de inetOrgPerson
/etc/ldap/schema/inetorgperson.schema     # Atributos de personas
/etc/ldap/schema/nis.schema               # Contiene posixAccount
```

## CAMBIOS EN EL ARCHIVO DOCKERFILE

Ahora el **servicio ldap** escucha por todos sus protocolos:

- Antes solo se exponía el puerto **TCP 389**, que es el del protocolo **ldap**. Los puertos **TCP 636** y **1636** (aunque según este link: https://www.openldap.org/doc/admin24/runningslapd.html solo es el 636) pertenecen al protocolo **ldaps** y el **IPC (Unix-domain socket)** a **ldapi**.

    ```
    # ldapserver
    # slapd es el demonio
    # ldap-utils son herramientas

    FROM debian
    LABEL subject="ldap server 2022"
    LABEL author="Isaac"
    ARG DEBIAN_FRONTEND=noninteractive
    RUN apt-get update && apt-get -y install procps tree nmap vim iproute2 iputils-ping slapd ldap-utils
    RUN mkdir /opt/docker
    WORKDIR /opt/docker
    COPY * /opt/docker/
    RUN chmod +x /opt/docker/startup.sh
    CMD /opt/docker/startup.sh
    EXPOSE 389
    EXPOSE 636                      # Esta orden es nueva.
    ```